//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Anand Yadav on 16/10/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Text("Welcome to Gujarat")
                .font(.title)
            HStack {
                Text("Vadodara")
                Spacer()
                Text("Subhanpura")
            }
            .font(.subheadline)
            .foregroundColor(.secondary)
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
