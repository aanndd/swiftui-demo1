//
//  SwiftUIDemoApp.swift
//  SwiftUIDemo
//
//  Created by Anand Yadav on 16/10/21.
//

import SwiftUI

@main
struct SwiftUIDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
